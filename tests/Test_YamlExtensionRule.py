import pytest

from ansiblelint.file_utils import Lintable
from ansiblelint.rules import RulesCollection
from ansiblelint.utils import find_children
from ansiblelint.runner import Runner
from rules.YamlExtensionRule import YamlExtensionRule

@pytest.fixture
def test_rules_collection() -> RulesCollection:
    """Instantiate a roles collection for tests."""
    collection = RulesCollection()
    collection.register(YamlExtensionRule())
    return collection

@pytest.mark.parametrize(
    ('filename', 'file_count', "match_count"),
    (
        pytest.param('tests/test_lint/YamlExtensionRule.yaml', 3, 2, id='yaml_extension'),
    ),
)

def test_YamlExtensionRule(
    test_rules_collection: RulesCollection,
    filename: str,
    file_count: int,
    match_count: int,
) -> None:
    """Check if number of loaded files is correct."""
    lintable = Lintable(filename, kind='playbook')
    runner = Runner(lintable, rules=test_rules_collection)
    result = runner.run()
    assert len(runner.lintables) == file_count
    assert len(result) == match_count
