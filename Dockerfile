FROM quay.io/ansible/default-test-container:3.4.0

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

RUN apt-get update -y \
  && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
  docker.io \
  && apt-get clean \
  && rm -r /var/lib/apt/lists/*

RUN mkdir /venv \
  && python3.6 -m venv /venv/py36 \
  && python3.8 -m venv /venv/py38

RUN /venv/py36/bin/pip install -U pip wheel \
  && /venv/py38/bin/pip install -U pip wheel \
  && /venv/py36/bin/pip install "ansible<2.10" molecule[docker] ansible-lint yamllint pytest \
  && /venv/py38/bin/pip install "ansible<2.10" molecule[docker] ansible-lint yamllint pytest \
  && rm -rf ~/.cache/pip
