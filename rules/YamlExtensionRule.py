import pytest
from ansiblelint.rules import AnsibleLintRule
from typing import List
from ansiblelint.constants import odict
from ansiblelint.errors import MatchError
from ansiblelint.file_utils import Lintable

class YamlExtensionRule(AnsibleLintRule):
    id = 'yaml-extension'
    shortdesc = 'Playbooks should have the ".yml" extension'
    description = ''
    tags = ['yaml']
    done = []  # already noticed path list

    def has_valid_yml_extension(self, path):
        """
        :param path: path to playbook file
    
        >>> YamlExtensionRule().has_valid_yml_extension('playbook.yml')
        True
        >>> YamlExtensionRule().has_valid_yml_extension('playbook.yaml')
        False
        >>> YamlExtensionRule().has_valid_yml_extension('')
        False
        """
        return path.endswith('.yml')

    def matchplay(self, file: Lintable, data: "odict[str, Any]") -> List[MatchError]:
        """
        Match Action
        """
        if not self.has_valid_yml_extension(str(file.path)):
            return [
                self.create_matcherror(
                    message="Playbook doesn't have '.yml' extension: " + str(data['__file__']) + ". " + self.shortdesc,
                    linenumber=data['__line__'],
                    filename=file
                )
            ]
        return []
